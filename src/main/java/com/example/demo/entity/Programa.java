package com.example.demo.entity;

public class Programa {
    private Integer programaId;

    private Integer order1;

    private String programa1Name;

    private Integer order2;

    private String programa2Name;

    public Integer getProgramaId() {
        return programaId;
    }

    public void setProgramaId(Integer programaId) {
        this.programaId = programaId;
    }

    public Integer getOrder1() {
        return order1;
    }

    public void setOrder1(Integer order1) {
        this.order1 = order1;
    }

    public String getPrograma1Name() {
        return programa1Name;
    }

    public void setPrograma1Name(String programa1Name) {
        this.programa1Name = programa1Name == null ? null : programa1Name.trim();
    }

    public Integer getOrder2() {
        return order2;
    }

    public void setOrder2(Integer order2) {
        this.order2 = order2;
    }

    public String getPrograma2Name() {
        return programa2Name;
    }

    public void setPrograma2Name(String programa2Name) {
        this.programa2Name = programa2Name == null ? null : programa2Name.trim();
    }
}