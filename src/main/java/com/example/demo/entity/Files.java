package com.example.demo.entity;

import java.util.Date;

public class Files {
    private Integer fileId;

    private String fileTitle;

    private String fileIntroduce;

    private String fileUrl;

    private String fileType;

    private String releaseStatus;

    private Integer programaId;

    private Date createtime;

    private String opeator;

    public Integer getFileId() {
        return fileId;
    }

    public void setFileId(Integer fileId) {
        this.fileId = fileId;
    }

    public String getFileTitle() {
        return fileTitle;
    }

    public void setFileTitle(String fileTitle) {
        this.fileTitle = fileTitle == null ? null : fileTitle.trim();
    }

    public String getFileIntroduce() {
        return fileIntroduce;
    }

    public void setFileIntroduce(String fileIntroduce) {
        this.fileIntroduce = fileIntroduce == null ? null : fileIntroduce.trim();
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl == null ? null : fileUrl.trim();
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType == null ? null : fileType.trim();
    }

    public String getReleaseStatus() {
        return releaseStatus;
    }

    public void setReleaseStatus(String releaseStatus) {
        this.releaseStatus = releaseStatus == null ? null : releaseStatus.trim();
    }

    public Integer getProgramaId() {
        return programaId;
    }

    public void setProgramaId(Integer programaId) {
        this.programaId = programaId;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public String getOpeator() {
        return opeator;
    }

    public void setOpeator(String opeator) {
        this.opeator = opeator == null ? null : opeator.trim();
    }
}