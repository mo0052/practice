package com.example.demo.dto;

import lombok.*;

import java.util.Date;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class UserinfoParam {
    private String userId;

    private String userType;

    private String userName;

    private String password;

    private Date registerTime;
}