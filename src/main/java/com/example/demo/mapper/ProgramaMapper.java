package com.example.demo.mapper;

import com.example.demo.entity.Programa;
import com.example.demo.entity.ProgramaExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface ProgramaMapper {
    int countByExample(ProgramaExample example);

    int deleteByExample(ProgramaExample example);

    int deleteByPrimaryKey(Integer programaId);

    int insert(Programa record);

    int insertSelective(Programa record);

    List<Programa> selectByExample(ProgramaExample example);

    Programa selectByPrimaryKey(Integer programaId);

    int updateByExampleSelective(@Param("record") Programa record, @Param("example") ProgramaExample example);

    int updateByExample(@Param("record") Programa record, @Param("example") ProgramaExample example);

    int updateByPrimaryKeySelective(Programa record);

    int updateByPrimaryKey(Programa record);
}