package com.example.demo.service.impl;

import com.example.demo.dto.UserinfoParam;
import com.example.demo.entity.Userinfo;
import com.example.demo.entity.UserinfoExample;
import com.example.demo.mapper.UserinfoMapper;
import com.example.demo.service.UserinfoService;
import com.example.demo.utils.JsonResult;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;

@Service
public class UserinfoServiceImpl implements UserinfoService {

    @Resource
    private UserinfoMapper userinfoMapper;


    //新增用户
    @Override
    public JsonResult insertUserInfo(@RequestBody UserinfoParam userinfoParam) {

        HashMap<String, Object> result = new HashMap<>();

        Userinfo userinfo = new Userinfo();

        BeanUtils.copyProperties(userinfoParam,userinfo);

        if (StringUtils.isEmpty(userinfo.getPassword() + "")) {
            result.put("info", "error");
            result.put("message", "密码不能为空");
            return new JsonResult(result);
        }

        if (userinfoMapper.insertSelective(userinfo)==1){
            result.put("info", "success");
            result.put("message", "新增题目成功");
            result.put("exerciseId", userinfo.getUserId());
            return new JsonResult(result);
        }

        result.put("info", "error");
        result.put("message", "新增用户信息失败");
        result.put("exerciseId", userinfo.getUserId());
        return new JsonResult(result);
    }

    //更新用户信息
    @Override
    public boolean updateUserInfo(Userinfo userinfo) {
        boolean flag=false;
        if (userinfoMapper.insertSelective(userinfo)==1){
            flag=true;
        }
        return flag;
    }

    //删除用户信息
    @Override
    public boolean deleteUserInfo(String userId) {
        boolean flag = false;
        if (userinfoMapper.deleteByPrimaryKey(userId) == 1) {
            flag = true;
        }
        return flag;
    }

    //获取用户信息
    @Override
    public Userinfo getUserInfo(String userId) {
        return userinfoMapper.selectByPrimaryKey(userId);
    }

    //获取全部用户信息
    @Override
    public List<Userinfo> getAllUserInfo() {
        UserinfoExample userinfoExample = new UserinfoExample();
        return userinfoMapper.selectByExample(userinfoExample);
    }
}
