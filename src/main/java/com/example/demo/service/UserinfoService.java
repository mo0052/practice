package com.example.demo.service;

import com.example.demo.dto.UserinfoParam;
import com.example.demo.entity.Userinfo;
import com.example.demo.utils.JsonResult;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

public interface UserinfoService {

    //新增用户
    JsonResult insertUserInfo(@RequestBody UserinfoParam userinfoParam);

    //更新用户信息
    boolean updateUserInfo(Userinfo userinfo);

    //删除用户信息
    boolean deleteUserInfo(String userId);

    //获取用户信息
    Userinfo getUserInfo(String userId);

    //获取全部用户信息
    List<Userinfo> getAllUserInfo();
}
