package com.example.demo.controller;

import com.example.demo.dto.UserinfoParam;
import com.example.demo.entity.Userinfo;
import com.example.demo.service.UserinfoService;
import com.example.demo.utils.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@CrossOrigin
@RequestMapping("/user")
@Api("用户信息模块")
public class UserinfoController {

    @Resource
    private UserinfoService userinfoService;

    @ApiOperation(value="获取用户详细信息", notes="根据url的userId来获取用户详细信息")
    @ApiImplicitParam(name = "userId", value = "用户ID", required = true, dataType = "String", paramType = "path")
    @RequestMapping(value = "/getUserInfo/{userId}",method = RequestMethod.GET)
    public JsonResult getUserInfo(@PathVariable(value = "userId") String userId){
        Userinfo userinfo=userinfoService.getUserInfo(userId);
        return new JsonResult(userinfo);
    }

    @ApiOperation(value = "插入或更新用户详细信息", notes = "没有此用户信息则插入用户信息，有此用户信息则更新用户信息")
    //@ApiImplicitParam(name = "userInfo", value = "用户实体", required = true, dataType = "UserInfo")
    @RequestMapping(value = "/updateUserInfo", method = RequestMethod.POST)
    public JsonResult updateUserInfo(@RequestBody UserinfoParam userinfoParam
    ) {
        return new JsonResult(userinfoService.insertUserInfo(userinfoParam));
    }

    @ApiOperation(value = "删除用户", notes = "根据url的userId来指定删除用户")
    @ApiImplicitParam(name = "userId", value = "用户ID", required = true, dataType = "String", paramType = "path")
    @RequestMapping(value = "/deleteUserInfo/{userId}", method = RequestMethod.DELETE)
    public JsonResult deleteUserInfo(@PathVariable(value = "userId") String userId) {
        if (userinfoService.deleteUserInfo(userId)) {
            return new JsonResult("删除用户成功");
        } else {
            return new JsonResult("删除用户成功");
        }
    }

    @ApiOperation(value = "获取全部用户信息", notes = "获取全部用户信息")
    @RequestMapping(value = "/getAllUserInfo", method = RequestMethod.GET)
    public JsonResult getAllUserInfo() {
        return new JsonResult(userinfoService.getAllUserInfo());
    }

}
